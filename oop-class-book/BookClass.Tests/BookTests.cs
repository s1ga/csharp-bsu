﻿using System;
using NUnit.Framework;

#pragma warning disable SA1600 // Elements should be documented

namespace BookClass.Tests
{
    [TestFixture]
    public class BookTests
    {
        [TestCase("Jon Skeet", "C# in Depth", "Manning Publications")]
        public void BookCreateNewThreeParameters(string author, string title, string publisher)
        {
            Book book = new Book(author, title, publisher);
            Assert.IsTrue(book.Author == author && book.Title == title && book.Publisher == publisher);
        }

        [TestCase("Jon Skeet", "C# in Depth", "Manning Publications", "978-0-901-69066-1")]
        [TestCase("Jon Skeet", "C# in Depth", "Manning Publications", "3-598-21508-8")]
        public void BookCreateNewFourParameters(string author, string title, string publisher, string isbn)
        {
            Book book = new Book(author, title, publisher, isbn);
            Assert.IsTrue(book.Author == author && book.Title == title && book.Publisher == publisher &&
                          book.ISBN == isbn);
        }

        [TestCase(null, "C# in Depth", "Manning Publications")]
        [TestCase("Jon Skeet", null, "Manning Publications")]
        [TestCase("Jon Skeet", "C# in Depth", null)]
        public void BookCreateNewThreeParametersThrowArgumentNullException(string author, string title,
                string publisher)
        {
            Assert.Throws<ArgumentNullException>(
                    () => new Book(author, title, publisher),
                    "author or title or publisher cannot be null");
        }

        [TestCase(null, "C# in Depth", "Manning Publications", "978-0-901-69066-1")]
        [TestCase("Jon Skeet", null, "Manning Publications", "978-0-901-69066-1")]
        [TestCase("Jon Skeet", "C# in Depth", null, "978-0-901-69066-1")]
        [TestCase("Jon Skeet", "C# in Depth", "Manning Publications", null)]
        public void BookCreateNewFourParametersThrowArgumentNullException(string author, string title, string publisher,
                string isbn)
        {
            Assert.Throws<ArgumentNullException>(
                    () => new Book(author, title, publisher, isbn),
                    "author or title or publisher or ISBN cannot be null");
        }

        [Test]
        public void BookPagesTest()
        {
            const int expected = 10;
            Book book = new Book(string.Empty, string.Empty, string.Empty) {Pages = expected};
            Assert.AreEqual(expected, book.Pages);
        }

        [TestCase(-1)]
        [TestCase(0)]
        public void BookPagesTestArgumentOutOfRangeException(int pages)
        {
            Book book = new Book(string.Empty, string.Empty, string.Empty);
            Assert.Throws<ArgumentOutOfRangeException>(() => book.Pages = pages,
                    "Count of pages should be greater than zero.");
        }

        [Test]
        public void BookPublishGetPublicationDateTests()
        {
            DateTime expected = DateTime.Now;
            Book book = new Book(string.Empty, string.Empty, string.Empty);
            book.Publish(expected);

            Assert.AreEqual(FormattableString.Invariant($"{expected:d}"), book.GetPublicationDate());
        }

        [Test]
        public void BookPublishGetPublicationDateEmptyTests()
        {
            const string expected = "NYP";
            Book book = new Book(string.Empty, string.Empty, string.Empty);
            Assert.AreEqual(expected, book.GetPublicationDate());
        }

        [Test]
        public void BookSetPriceTests()
        {
            const decimal price = 4.44m;
            const string currency = "USD";
            Book book = new Book(string.Empty, string.Empty, string.Empty);
            book.SetPrice(price, currency);
            Assert.IsTrue(book.Price == price && book.Currency == currency);
        }

        [Test]
        public void BookSetPriceInvalidCurrencyArgumentException()
        {
            const decimal price = 4.44m;
            const string currency = "_~_";
            Book book = new Book(string.Empty, string.Empty, string.Empty);

            Assert.Throws<ArgumentException>(() => book.SetPrice(price, currency), "Currency is invalid.");
        }

        [Test]
        public void BookSetPriceLessZeroArgumentException()
        {
            const decimal price = -4.44m;
            const string currency = "USD";
            Book book = new Book(string.Empty, string.Empty, string.Empty);

            Assert.Throws<ArgumentException>(() => book.SetPrice(price, currency), "Price cannot be less than zero.");
        }

        [Test]
        public void BookSetPriceCurrencyIsNullArgumentNullException()
        {
            const decimal price = 4.44m;
            Book book = new Book(string.Empty, string.Empty, string.Empty);

            Assert.Throws<ArgumentNullException>(() => book.SetPrice(price, null), "Currency cannot be null");
        }

        [TestCase("Jon Skeet", "C# in Depth", "Manning Publications", ExpectedResult = "C# in Depth by Jon Skeet")]
        [TestCase("Jon Skeet", "", "Manning Publications", ExpectedResult = " by Jon Skeet")]
        [TestCase("", "C# in Depth", "Manning Publications", ExpectedResult = "C# in Depth by ")]
        public string BookToString(string author, string title, string publisher) =>
                new Book(author, title, publisher).ToString();

        [Test]
        public void BookPublishTest()
        {
            const int year = 2011;
            const int month = 11;
            const int day = 11;
            DateTime date = new DateTime(year, month, day);

            Book book = new Book("Author", "Title", "Publisher");
            book.Publish(date);
            Assert.AreEqual(book.GetPublicationDate(), $"{month}/{day}/{year}");
        }

        [Test]
        public void BookPublishNotPublishedTest()
        {
            Book book = new Book("Author", "Title", "Publisher");
            Assert.AreEqual(book.GetPublicationDate(), "NYP");
        }
        
        [TestCase("978-7-460-37289-2", "978-7-460-37289-2", ExpectedResult = true)]
        [TestCase("512-4-676-89127-0", "978-7-460-37289-2", ExpectedResult = false)]
        public bool BookEquals(string isbn1, string isbn2) =>
                new Book(string.Empty, string.Empty, string.Empty, isbn1)
                        .Equals(new Book(string.Empty, string.Empty, string.Empty, isbn2));
       
        [TestCase("978-7-460-37289-2", ExpectedResult = false)]
        public bool BookEqualsNull(string isbn1) =>
                new Book(string.Empty, string.Empty, string.Empty, isbn1)
                        .Equals(null);
        
        [TestCase("Title", "Titl", ExpectedResult = -1)]
        [TestCase("Title", "Title", ExpectedResult = 0)]
        public int BookComparison(string title1, string title2) =>
                new Book(string.Empty, title1, string.Empty)
                        .CompareTo(new Book(string.Empty, title2, string.Empty));
        
        [TestCase("C# in Depth", "C# in Depth", ExpectedResult = 0)]
        [TestCase("C# in Depth", "C++ in Depth", ExpectedResult = 1)]
        [TestCase("C# in Depth", "C in Depth", ExpectedResult = -1)]
        public int BookTitleComparisonTest(string title1, string title2) =>
                new Book(string.Empty, title1, string.Empty)
                        .CompareTo(new Book(string.Empty, title2, string.Empty));

        [TestCase(123, 1123, ExpectedResult = -1)]
        [TestCase(10, 10, ExpectedResult = 0)]
        [TestCase(123, 12, ExpectedResult = 1)]
        public int BookPagesComparisonTest(int firstBookPages, int secondBookPages) =>
                new BookPagesComparator().Compare(
                        new Book(string.Empty, string.Empty, string.Empty) {Pages = firstBookPages},
                        new Book(string.Empty, string.Empty, string.Empty) {Pages = secondBookPages});

        [TestCase(1, "USD", 2, "USD", ExpectedResult = -1)]
        [TestCase(1, "USD", 1, "USD", ExpectedResult = 0)]
        [TestCase(1, "USD", 0, "USD", ExpectedResult = 1)]
        public int BookPriceComparisonTest(int firstBookPrice, string firstBookPriceCurrency, int secondBookPrice,
                string secondBookPriceCurrency)
        {
            Book firstBook = new Book(string.Empty, string.Empty, string.Empty);
            firstBook.SetPrice(firstBookPrice, firstBookPriceCurrency);

            Book secondBook = new Book(string.Empty, string.Empty, string.Empty);
            secondBook.SetPrice(secondBookPrice, secondBookPriceCurrency);

            return new BookPriceComparator().Compare(firstBook, secondBook);
        }

        [TestCase("Jon Skee", "Jon Skeet", ExpectedResult = -1)]
        [TestCase("Jon Skeet", "Jon Skeet", ExpectedResult = 0)]
        [TestCase("Jon Skeet", "Jon Skee", ExpectedResult = 1)]
        public int BookAuthorComparisonTest(string firstAuthor, string secondAuthor) =>
                new BookAuthorComparator().Compare(
                        new Book(firstAuthor, string.Empty, string.Empty),
                        new Book(secondAuthor, string.Empty, string.Empty)
                );
    }
}
