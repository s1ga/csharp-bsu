﻿using System.Collections.Generic;
using BookClass;

namespace BookListService
{
    public class BookStorage
    {
        private List<Book> books;

        public BookStorage()
        {
            this.books = new List<Book>();
        }

        public bool IsEmpty()
        {
            return this.books.Count == 0;
        }

        public List<Book> GetBooks()
        {
            return books;
        }

        public void AddBooks(List<Book> booksToAdd)
        {
            foreach (var book in booksToAdd)
            {
                this.books.Add(book);
            }
        }
    }
}
