﻿using System;
using NUnit.Framework;

#pragma warning disable SA1600 // Elements should be documented

namespace VerificationService.Tests
{
    [TestFixture]
    public class VerificationTests
    {
        [TestCase("1-111-11111-1")]
        [TestCase("2-123-45678-0")]
        public void IsbnVerifierIsValidIsbn10(string isbn10)
        {
            Assert.IsTrue(IsbnVerifier.IsValid(isbn10));
        }

        [TestCase("512-4-676-89127-0")]
        [TestCase("581-5-123-75907-5")]
        public void IsbnVerifierIsValidIsbn13(string isbn13)
        {
            Assert.IsFalse(IsbnVerifier.IsValid(isbn13));
        }

        [TestCase("444-4-312-34534")]
        [TestCase("3133")]
        [TestCase("b-34v-a4444-4")]
        [TestCase("a33-2-e67-33333-3")]
        public void IsbnVerifierIsNotValid(string isbn)
        {
            Assert.IsFalse(IsbnVerifier.IsValid(isbn));
        }

        [TestCase("USD")]
        [TestCase("EUR")]
        public void IsoCurrencyValidatorTests(string currency)
        {
            Assert.IsTrue(IsoCurrencyValidator.IsValid(currency));
        }

        [TestCase("-+-")]
        [TestCase("ddd")]
        [TestCase("SAC")]
        public void IsoCurrencyValidatorArgumentException(string currency)
        {
            Assert.Throws<ArgumentException>(() => IsoCurrencyValidator.IsValid(currency), "not ISO format");
        }
    }
}
