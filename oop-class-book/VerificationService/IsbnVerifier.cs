﻿using System;

namespace VerificationService
{
    /// <summary>
    /// Verifies if the string representation of number is a valid ISBN-10 or ISBN-13 identification number of book.
    /// </summary>
    public static class IsbnVerifier
    {
        /// <summary>
        /// Verifies if the string representation of number is a valid ISBN-10 or ISBN-13 identification number of book.
        /// </summary>
        /// <param name="isbn">The string representation of book's isbn.</param>
        /// <returns>true if number is a valid ISBN-10 or ISBN-13 identification number of book, false otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown if isbn is null.</exception>
        public static bool IsValid(string isbn)
        {
            if (isbn is null)
            {
                throw new ArgumentNullException();
            }

            return isbn.Length switch
            {
                    13 => IsValidISBN10(isbn),
                    17 => IsValidISBN13(isbn),
                    _ => false
            };
        }

        private static bool IsValidISBN10(string isbn)
        {
            if (string.IsNullOrEmpty(isbn))
            {
                return false;
            }

            long j;
            if (isbn.Contains('-'))
            {
                isbn = isbn.Replace("-", "");
            }

            if (!Int64.TryParse(isbn[..^1], out j))
            {
                return false;
            }

            char lastChar = isbn[^1];
            if (lastChar == 'X' && !Int64.TryParse(lastChar.ToString(), out j))
            {
                return false;
            }
           
            int sum = 0;
            for (int i = 0; i < 9; i++)
                sum += Int32.Parse(isbn[i].ToString()) * (i + 1);

            return sum % 11 == int.Parse(isbn[9].ToString());
        }

        private static bool IsValidISBN13(string isbn)
        {
            if (string.IsNullOrEmpty(isbn))
            {
                return false;
            }

            long j;
            if (isbn.Contains('-'))
            {
                isbn = isbn.Replace("-", "");
            }
            if (!Int64.TryParse(isbn, out j))
            {
                return false;
            }
            
            int sum = 0;
            for (int i = 0; i < 12; i++)
            {
                sum += Int32.Parse(isbn[i].ToString()) * (i % 2 == 1 ? 3 : 1);
            }

            int remainder = sum % 10;
            int checkDigit = 10 - remainder;
            if (checkDigit == 10)
            {
                checkDigit = 0;
            }

            return checkDigit == int.Parse(isbn[12].ToString());
        }
    }
}
