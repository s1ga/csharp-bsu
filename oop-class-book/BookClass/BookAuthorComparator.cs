using System;
using System.Collections.Generic;

namespace BookClass
{
    /// <summary>Book Author Comparator</summary>
    public class BookAuthorComparator : IComparer<Book>
    {
        /// <summary>Compare two books by author.</summary>
        /// <param name="firstBook">First book to compare.</param>
        /// <param name="secondBook">Second book to compare.</param>
        /// <returns>int.</returns>
        public int Compare(Book firstBook, Book secondBook)
        {
            return firstBook switch
            {
                    null when secondBook == null => 0,
                    null => -1,
                    _ => secondBook == null
                            ? 1
                            : string.Compare(firstBook.Author, secondBook.Author, StringComparison.Ordinal)
            };
        }
    }
}
