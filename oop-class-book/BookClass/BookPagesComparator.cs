using System;
using System.Collections.Generic;

namespace BookClass
{
    /// <summary>Book Pages Comparator.</summary>
    public class BookPagesComparator : IComparer<Book>
    {
        /// <summary>Compare two books by pages.</summary>
        /// <param name="firstBook">First book to compare.</param>
        /// <param name="secondBook">Second book to compare.</param>
        /// <returns>int.</returns>
        public int Compare(Book firstBook, Book secondBook)
        {
            return firstBook switch
            {
                    null when secondBook == null => 0,
                    null => -1,
                    _ => secondBook == null ? 1 : firstBook.Pages.CompareTo(secondBook.Pages)
            };
        }
    }
}
