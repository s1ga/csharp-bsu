﻿using System;
using System.Collections.Generic;
using System.Linq;
using BookClass;

namespace BookListService
{
    public class BookListService
    {
        private Dictionary<int, Book> books = new Dictionary<int, Book>();

        public bool IsEmpty()
        {
            return this.books.Count == 0;
        }
        
        public Dictionary<int, Book> GetBookList()
        {
            return this.books;
        }

        public List<Book> GetByAuthor()
        {
            List<Book> bookList = this.books.Select(pair => pair.Value).ToList();

            bookList.Sort(new BookAuthorComparator().Compare);
            return bookList;
        }

        public List<Book> GetByPages()
        {
            List<Book> bookList = this.books.Select(pair => pair.Value).ToList();

            bookList.Sort(new BookPagesComparator().Compare);
            return bookList;
        }

        public List<Book> GetByPrice()
        {
            List<Book> bookList = this.books.Select(pair => pair.Value).ToList();

            bookList.Sort(new BookPriceComparator().Compare);
            return bookList;
        }

        public List<Book> FindByAuthor(string author)
        {
            return this.books
                    .Select(pair => pair.Value)
                    .Where(book => book.Author.Equals(author))
                    .ToList();
        }

        public List<Book> FindByTitle(string title)
        {
            return this.books
                    .Select(pair => pair.Value)
                    .Where(book => book.Title.Equals(title))
                    .ToList();
        }

        public List<Book> FindByPublisher(string publisher)
        {
            return this.books
                    .Select(pair => pair.Value)
                    .Where(book => book.Publisher.Equals(publisher))
                    .ToList();
        }

        public void Add(Book book)
        {
            if (!this.books.ContainsValue(book))
            {
                this.books.Add(this.books.Count, book);
            }
            else
            {
                throw new Exception("This book is already in store");
            }
        }

        public void Remove(Book book)
        {
            if (!this.books.ContainsValue(book))
            {
                throw new Exception("There is no such book");
            }

            int keyToRemove = -1;
            foreach (var pair in this.books.Where(pair => pair.Value.Equals(book)))
            {
                keyToRemove = pair.Key;
                break;
            }

            this.books.Remove(keyToRemove);
        }

        public void Load(BookStorage bookStorage)
        {
            List<Book> bookList = bookStorage.GetBooks();
            int count = this.books.Count;
            
            for (int i = 0; i < bookList.Count; i++)
            {
                this.books.Add(i + count, bookList[i]);
            }
        }

        public void Save(BookStorage bookStorage)
        {
            List<Book> booksList = this.books.Select(pair => pair.Value).ToList();
            bookStorage.AddBooks(booksList);
        }
    }
}
