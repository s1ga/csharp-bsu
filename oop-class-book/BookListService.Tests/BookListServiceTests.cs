﻿using System;
using System.Collections.Generic;
using BookClass;
using NUnit.Framework;

#pragma warning disable SA1600 // Elements should be documented

namespace BookListService.Tests
{
    public class BookListServiceTests
    {
        [TestCase("Jon Skeet", "C# in Depth", "Manning Publications")]
        public void BookServiceAdd(string author, string title, string publisher)
        {
            BookListService bookListService = new BookListService();
            bookListService.Add(new Book(author, title, publisher));

            Assert.IsFalse(bookListService.IsEmpty());
        }

        [TestCase("Jon Skeet", "C# in Depth", "Manning Publications")]
        public void BookServiceAddException(string author, string title, string publisher)
        {
            BookListService bookListService = new BookListService();
            Book book = new Book(author, title, publisher);

            bookListService.Add(book);
            Assert.Throws<Exception>(() => bookListService.Add(book), "This book is already in store");
        }

        [TestCase("Jon Skeet", "C# in Depth", "Manning Publications")]
        public void BookServiceRemove(string author, string title, string publisher)
        {
            BookListService bookListService = new BookListService();
            Book book = new Book(author, title, publisher);

            bookListService.Add(book);
            bookListService.Remove(book);
            Assert.IsTrue(bookListService.IsEmpty());
        }

        [TestCase("Jon Skeet", "C# in Depth", "Manning Publications")]
        public void BookServiceRemoveException(string author, string title, string publisher)
        {
            BookListService bookListService = new BookListService();
            Book book = new Book(author, title, publisher);

            Assert.Throws<Exception>(() => bookListService.Remove(book), "There is no such book");
        }

        [Test]
        public void BookServiceLoad()
        {
            BookListService bookListService = new BookListService();
            Book book1 = new Book("A", string.Empty, string.Empty);
            Book book2 = new Book("B", string.Empty, string.Empty);
            Book book3 = new Book("C", string.Empty, string.Empty);

            BookStorage bookStorage = new BookStorage();
            bookStorage.AddBooks(new List<Book>() {book1, book2, book3});

            bookListService.Load(bookStorage);
            
            Assert.IsTrue(bookListService.GetBookList().Count == bookStorage.GetBooks().Count);
        }
        
        [Test]
        public void BookServiceSave()
        {
            BookListService bookListService = new BookListService();
            Book book1 = new Book("A", string.Empty, string.Empty);
            Book book2 = new Book("B", string.Empty, string.Empty);
            Book book3 = new Book("C", string.Empty, string.Empty);
            
            bookListService.Add(book1);
            bookListService.Add(book2);
            bookListService.Add(book3);
           
            BookStorage bookStorage = new BookStorage();
            bookListService.Save(bookStorage);
            
            Assert.IsTrue(bookListService.GetBookList().Count == bookStorage.GetBooks().Count);
        }
        
        [TestCase("Jon Skeet")]
        public void BookServiceFindByAuthor(string author)
        {
            BookListService bookListService = new BookListService();

            Book book1 = new Book(string.Empty, string.Empty, string.Empty);
            Book book2 = new Book(author, string.Empty, string.Empty);

            bookListService.Add(book1);
            bookListService.Add(book2);

            List<Book> list = bookListService.FindByAuthor(author);

            Assert.IsTrue(list.Contains(book2) && list.Count == 1);
        }

        [TestCase("C# in Depth")]
        public void BookServiceFindByTitle(string title)
        {
            BookListService bookListService = new BookListService();
            
            Book book1 = new Book(string.Empty, title, string.Empty);
            Book book2 = new Book(string.Empty, string.Empty, string.Empty);

            bookListService.Add(book1);
            bookListService.Add(book2);

            List<Book> list = bookListService.FindByTitle(title);

            Assert.IsTrue(list.Contains(book1) && list.Count == 1);
        }

        [TestCase("Manning Publications")]
        public void BookServiceFindByPublisher(string publisher)
        {
            BookListService bookListService = new BookListService();

            Book book1 = new Book(string.Empty, string.Empty, publisher);
            Book book2 = new Book(string.Empty, string.Empty, string.Empty);

            bookListService.Add(book1);
            bookListService.Add(book2);

            List<Book> list = bookListService.FindByPublisher(publisher);

            Assert.IsTrue(list.Contains(book1) && list.Count == 1);
        }

        [TestCase("C", "A", "B")]
        public void BookServiceGetByAuthor(string author1, string author2, string author3)
        {
            BookListService bookListService = new BookListService();

            Book book1 = new Book(author1, string.Empty, string.Empty);
            Book book2 = new Book(author2, string.Empty, string.Empty);
            Book book3 = new Book(author3, string.Empty, string.Empty);
   
            bookListService.Add(book1);
            bookListService.Add(book2);
            bookListService.Add(book3);

            List<Book> list = bookListService.GetByAuthor();

            Assert.IsTrue(list[0].Equals(book2));
        }

        [TestCase(4, 2, 3)]
        public void BookServiceGetByPages(int pages1, int pages2, int pages3)
        {
            BookListService bookListService = new BookListService();

            Book book1 = new Book("B", string.Empty, string.Empty) { Pages = pages1 };
            Book book2 = new Book("A", string.Empty, string.Empty) { Pages = pages2 };
            Book book3 = new Book("C", string.Empty, string.Empty) { Pages = pages3 };

            bookListService.Add(book1);
            bookListService.Add(book2);
            bookListService.Add(book3);

            List<Book> list = bookListService.GetByPages();

            Assert.IsTrue(list[0].Equals(book2));
        }

        [TestCase(4, 2, 3, "USD")]
        public void BookServiceGetByPrice(int price1, int price2, int price3, string currency)
        {
            BookListService bookListService = new BookListService();

            Book book1 = new Book("B", string.Empty, string.Empty);
            Book book2 = new Book("A", string.Empty, string.Empty);
            Book book3 = new Book("C", string.Empty, string.Empty);

            book1.SetPrice(price1, currency);
            book2.SetPrice(price2, currency);
            book3.SetPrice(price3, currency);

            bookListService.Add(book1);
            bookListService.Add(book2);
            bookListService.Add(book3);

            List<Book> list = bookListService.GetByPrice();

            Assert.IsTrue(list[0].Equals(book2));
        }
    }
}
